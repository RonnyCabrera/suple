//
//  Network.swift
//  Suple
//
//  Created by Ronny Cabrera on 21/8/18.
//  Copyright © 2018 epn. All rights reserved.
//

import Foundation
import Alamofire

class Network {
    func getAllPerson(completion:@escaping ([Person])->()) {
        var personArray:[Person] = []
        Alamofire.request("https://private-534ec-amstronghuang.apiary-mock.com/members").responseJSON {
            response in
            guard let data = response.data else {
                print("ERROR")
                return
            }
            
            guard let person = try? JSONDecoder().decode(JsonInfo.self, from: data) else {
                print("error decoding Person")
                return
            }
            
            for people in person.data {
                let personAux = Person(firstName: people.firstName, lastName: people.lastName, email: people.email, address: people.address)
            
                
                     personArray.append(personAux)
                
                
                
            }
         
            completion(personArray)
        }
    }
}
