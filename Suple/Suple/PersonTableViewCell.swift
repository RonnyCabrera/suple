//
//  PersonTableViewCell.swift
//  Suple
//
//  Created by Ronny Cabrera on 21/8/18.
//  Copyright © 2018 epn. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(label: Person) {
        firstNameLabel.text = label.firstName
        lastNameLabel.text = label.lastName
        emailLabel.text = label.email
    }

}
